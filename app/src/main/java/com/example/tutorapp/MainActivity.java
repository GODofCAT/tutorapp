package com.example.tutorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ViewFlipper;

public class MainActivity extends AppCompatActivity {

    Button butttonNext,butttonPrevious;
    ViewFlipper viewFlipperPages;
    int currentPageId;int countOfLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        butttonNext = findViewById(R.id.butttonNext);
        butttonPrevious = findViewById(R.id.butttonPrevious);

        butttonNext.setOnClickListener(buttonNextOnClick);
        butttonPrevious.setOnClickListener(buttonPreviousOnClick);

        viewFlipperPages = findViewById(R.id.viewFlipperPages);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        int[] layoutIds = new int[]{R.layout.first_tip,R.layout.second_tip};
        countOfLayout = layoutIds.length;

        for (int layoutId:layoutIds){
            viewFlipperPages.addView(inflater.inflate(layoutId,null));
        }



        currentPageId=0;

    }

    View.OnClickListener buttonNextOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (currentPageId==countOfLayout-1){
                finish();
            }

            viewFlipperPages.showNext();
            currentPageId++;
        }
    };

    View.OnClickListener buttonPreviousOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (currentPageId>0){
                viewFlipperPages.showPrevious();
                currentPageId--;
            }
        }
    };
}